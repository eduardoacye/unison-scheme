(import (scheme re-lang))

(define :list-open: (re "("))

(define :list-close: (re ")"))

(define :vector-open: (re "#("))

(define :bytevector-open: (re "#u8("))

(define :quote: (re "'"))

(define :quasiquote: (re "`"))

(define :unquote: (re ","))

(define :unquote-splicing: (re ",@"))

(define :dot: (re "."))

(define :boolean:
  (re (: "#t" "#f" "#true" "#false")))

(define :character:
  (re (: #\null #\alarm #\backspace #\tab #\newline #\return #\escape #\space #\delete
	 #\! #\" #\# #\$ #\% #\& #\' #\( #\) #\* #\+ #\, #\- #\. #\/
	 #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9
	 #\: #\; #\< #\= #\> #\? #\@
	 #\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M
	 #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
	 #\[ #\\ #\] #\^ #\_ #\`
	 #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m
	 #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z
	 #\{ #\| #\} #\~)))


(define :string:
  (re (.. #\"
	  (: #\null #\alarm #\backspace #\tab #\newline #\return #\escape #\space #\delete
	     #\! #\# #\$ #\% #\& #\' #\( #\) #\* #\+ #\, #\- #\. #\/
	     #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9
	     #\: #\; #\< #\= #\> #\? #\@
	     #\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M
	     #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
	     #\[ #\] #\^ #\_ #\`
	     #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m
	     #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z
	     #\{ #\| #\} #\~
	     "\"" "\\"
	     (.. #\\
		 (* (: #\space #\tab))
		 (: #\newline #\return (.. #\return #\newline))
		 (* (: #\space #\tab))))
	  #\")))

(define :identifier:
  (re (: (.. (: #\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M
		#\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
		#\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m
		#\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z
		#\! #\$ #\% #\& #\* #\/ #\: #\< #\= #\> #\? #\^ #\_ #\~)
	     (* (: #\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M
		   #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
		   #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m
		   #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z
		   #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\. #\+ #\- #\@)))
	 (.. #\|
	     (* (: #\null #\alarm #\backspace #\tab #\newline #\return #\escape #\space #\delete
		   #\! #\" #\# #\$ #\% #\& #\' #\( #\) #\* #\+ #\, #\- #\. #\/
		   #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9
		   #\: #\; #\< #\= #\> #\? #\@
		   #\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M
		   #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z
		   #\[ #\] #\^ #\_ #\`
		   #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m
		   #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z
		   #\{ #\} #\~
		   "\|" (.. #\\ (: #\a #\b #\t #\n #\r))))
	     #\|))))

(define :number:
  (re (.. (: "+" "-" #t)
	  (: "1" "2" "3" "4" "5" "6" "7" "8" "9")
	  (* (: "0" "1" "2" "3" "4" "5" "6" "7" "8" "9")))))
