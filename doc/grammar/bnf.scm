;;; <x> es el no terminal x
;;; :x: es el token x
;;; -> es una derivación
;;; or es equivalente a |

<datum> -> <simple-datum>
        or <compound-datum>

<simple-datum> -> :boolean:
               or :number:
               or :character:
               or :string:
               or <symbol>

<symbol> -> :identifier:

<compound-datum> -> <list>
                 or <vector>

<list> -> :(: <list2>
       or <abbreviation>

<list2> -> <datum-list> <list3>

<list3> -> :):
        or :boolean: :.: <datum> :):
        or :number: :.: <datum> :):
        or :character: :.: <datum> :):
        or :string: :.: <datum> :):
        or :identifier: :.: <datum> :):
        or <vector> :.: <datum> :):
        or :(: <list2> :.: <datum> :):
        or <abbreviation> :.: <datum> :):

<abbreviation> -> <abbrev-prefix> <datum>

<abbrev-prefix> -> :':
                or :`:
                or :,:
                or :,@:

<vector> -> :#(: <datum-list> :):

<datum-list> -> epsilon
             or :boolean: <datum-list>
             or :number: <datum-list>
             or :character: <datum-list>
             or :string: <datum-list>
             or :identifier: <datum-list>
             or :#(: <datum-list> :): <datum-list>
             or :(: <list2> <datum-list>
             or :': <datum> <datum-list>
             or :`: <datum> <datum-list>
             or :,: <datum> <datum-list>
             or :,@: <datum> <datum-list>






























