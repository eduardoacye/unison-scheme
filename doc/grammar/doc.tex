\documentclass[12pt,letterpaper,draft]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish, es-noquoting]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{fourier}
\usepackage{fancyvrb}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\author{Eduardo Acuña Yeomans}
\title{\textbf{Compiladores}\\Lenguaje a compilar: Scheme}
\date{}
\begin{document}
\maketitle

En este documento se describe el lenguaje Scheme que se compilará, incluyendo sus propiedades y su sintaxis.

El lenguaje Scheme es un dialecto de Lisp y al igual que la mayoría de sus parientes cercanos, emplea una notación en prefijo completamente entre paréntesis, esto es tanto para la notación de los programas como para la notación de los datos que manejan los programas; la gramática de Scheme genera un sublenguaje del lenguaje de los datos que se pueden manipular, esta característica implica que la representación entre los programas y los datos es uniforme y por lo tanto, los programas de Scheme pueden ser idiomáticamente manipulados por otros programas de Scheme.

\section*{Propiedades de Scheme}

Si bien Scheme es un lenguaje de programación cercano en espíritu a los lenguajes de programación funcionales, tiene algunas similaridades con lenguajes mas convencionales como C. La principal coincidencia entre Scheme y C es que ambos son de ámbito léxico y estructurado en bloques. Que tenga ámbito léxico implica que el cuerpo del código, o ámbito, en la que una variable es visible depende solo de la estructura del código y no en la naturaleza dinámica del cómputo que realiza. Que sea estructurado en bloques implica que los ámbitos pueden anidarse (en bloques); En Scheme cualquier expresión puede introducir un nuevo bloque con sus propias variables locales que son visibles solo dentro de ese bloque.

Scheme es un lenguaje con evaluación estricta, es decir, en una llamada a función, tanto la función como los argumentos son evaluados antes de la aplicación.

En Scheme las funciones son "ciudadanos de primera clase", también llamadas clausuras, estas son objetos que combina un procedimiento con una asociación léxica de variables libres y valores en el contexto en que la clausura fue creada; estos objetos pueden ser recibidos como argumentos o regresados como valores de otras funciones.

Scheme es un lenguaje débilmente tipado, esto implica que se determina si los tipos son correctos al momento se ejecutarse el cómputo en lugar de al momento de compilado.

\section*{Sintaxis del lenguaje}

La sintaxis de las expresiones en Scheme se compone de un conjunto de formas sintácticas básicas y de un conjunto de extensiones sintácticas las cuales pueden ser definidas en términos de las formas sintácticas básicas. De tal manera que un sistema de Scheme reducido pudiera solo soportar las formas sintácticas básicas y por medio de mecanismos de transformación, soportar las extensiones sintácticas como derivaciones de funciones y formas sintácticas básicas.

La sintaxis de Scheme en esencia se ha mantenido igual a lo largo de los estándares, sin embargo, con el pasar del tiempo, la sintaxis se ha expandido poco a poco hasta comprender formas extendidas sofisticadas y tipos de datos útiles y eficientes. Consideraré para este proyecto la sintaxis formal de la tercera revisión de Scheme ($R^3RS$), teniendo claro que después de programar una implementación que funcione, la sintaxis será enriquecida con los agregados de las revisiones mas recientes.

Como se mencionó previamente, cualquier programa de Scheme se puede considerar también como datos de Scheme. Se tiene soporte para listas (e.g. \texttt{(a b c)}), símbolos (e.g. \texttt{xyz}), enteros (e.g. \texttt{12345}), cadenas (e.g. \texttt{"hola mundo"}) y otros tipos de objetos. A su vez, las variables en Scheme son símbolos a los cuales se les asocia un valor, las expresiones compuestas son listas y las constantes son números, cadenas y otros tipos de objetos.

\subsection*{Estructura léxica}
A continuación se describe como tokens individuales son formados a partir de secuencias de caracteres. La categoría sintáctica $\langle \text{intertoken space} \rangle$ puede ocurrir en cualquier lado de cualquier token, pero no dentro de un token.

\begin{Verbatim}[fontsize=\small]
<token> -> <identifier> | <boolean> | <number> | <character> | <string>
         | ( | ) | #( | ' | ` | , | ,@ | . 
<delimiter> -> <whitespace> | ( | ) | " | ;
<whitespace> -> <space or newline>
<comment> -> ; <all subsequent characters up to a line break>
<atmosphere> -> <whitespace> | <comment>
<intertoken space> -> <atmosphere>*

<identifier> -> <initial> <subsequent>* | <peculiar identifier>
<initial> -> <letter> | <special initial>
<letter> -> a | b | ... | y | z | A | B | ... | Y | Z
<special initial> -> ! | $ | % | & | * | / | : | < | = | > | ? | ~ | _ | ^
<subsequent> -> <initial> | <digit> | <special subsequent>
<digit> -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
<special subsequent> -> . | + | -
<peculiar identifier> -> + | -
<syntactic keyword> -> <expression keyword> | else | => | define | unquote
                     | unquote-splicing
<expression keyword> -> quote | lambda | if | set! | begin | cond | and | or | case | let
                      | let* | letrec | do | delay | quasiquote

<variable> -> <cualquier <identifier> que no sea <syntactic keyword>>

<boolean> -> #t | #f
<character> -> #\ <any character> | #\ <character name>
<character name> -> space | newline

<string> -> " <string element>*  "
<string element> -> <cualquier caracter que no sea " o \> | \" | \\

<number> -> <real> | <real> + <ureal> i | <real> - <ureal> i | <real> @ <real>
<real> -> <sign> <ureal>
<ureal> -> <ureal 2> | <ureal 8> | <ureal 10> | <ureal 16>

<ureal R> -> <prefix R> <uinteger R> <suffix>
           | <prefix R> <uinteger R> / <uinteger R> <suffix>
           | <prefix R> . <digit R>+ #* <suffix>
           | <prefix R> <digit R>+ . <digit R>* #* <suffix>
           | <prefix R> <digit R>+ #* . #* <suffix>
<prefix R> -> <radix R> <exactness> <precision>
            | <radix R> <precision> <exactness>
            | <exactness> <radix R> <precision>
            | <exactness> <precision> <radix R>
            | <precision> <radix R> <exactness>
            | <precision> <exactness> <radix R>
<uinteger R> -> <digit R>+ #*

<sign> -> <empty> | + | -
<suffix> -> <empty> | e <sign> <digit>+
<exactness> -> <empty> | #i | #e
<precision> -> <empty> | #s | #l
<radix 2> -> #b
<radix 8> -> #o
<radix 10> -> <empty> | #d
<radix 16> -> #x
<digit 2> -> 0 | 1
<digit 8> -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7
<digit 10> -> <digit>
<digit 16> -> <digit> | a | b | c | d | e | f
\end{Verbatim}

\subsection*{Representaci\'on externa}
La etapa de análisis sintáctico debe producir una estructura parecida a la de una \emph{expresión-S}, estas expresiones son la representación tanto del código como de datos. Las operaciones para manipular y procesar estas expresiones son parte del lenguaje. Debido a esto, podemos manipular programas fácilmente desde otros programas (ver \textbf{homoiconicidad}). Para procesar código y obtener una \emph{expresión-S}, se utiliza la función \texttt{read}. La representación externa de los datos determina la estructura de las expresiones-S en Scheme. $\langle$Datum$\rangle$ es la categoria sintáctica que representa lo que parsea la función \texttt{read}.

\begin{Verbatim}[fontsize=\small]
<datum> -> <simple datum> | <compound datum>
<simple datum> -> <boolean> | <number> | <character> | <string> | <symbol>
<symbol> -> <identifier>
<compound datum> -> <list> | <vector>
<list> -> (<datum>*) | (<datum>+ . <datum>) | <abbreviation>
<abbreviation> -> <abbrev prefix> <datum>
<abbrev prefix> -> ' | ` | , | ,@
<vector> -> #(<datum>*)
\end{Verbatim}

\newpage
\subsection*{Expresiones}
Cualquier cadena que pueda ser parseada como $\langle$expression$\rangle$ ser\'a parseada como $\langle$Datum$\rangle$.

\begin{Verbatim}[fontsize=\small]
<expression> -> <variable>
              | <literal>
              | <procedure call>
              | <lambda expression>
              | <conditional>
              | <assignment>
              | <derived expression>

<literal> -> <quotation> | <self-evaluating>
<self-evaluating> -> <boolean> | <number> | <character> | <string>
<quotation> -> '<datum> | (quote <datum>)
<procedure call> -> (<operator> <operand>*)
<operator> -> <expression>
<operand> -> <expression>

<lambda expression> -> (lambda <formals> <body>)
<formals> -> (<variable>*) | <variable> | (<variable>+ . <variable>)
<body> -> <definition>* <sequence>
<sequence> -> <command>* <expression>
<command> -> <expression>

<conditional> -> (if <test> <consequent> <alternate>)
<test> -> <expression>
<consequent> -> <expression>
<alternate> -> <expression> | <empty>

<assignment> -> (set! <variable> <expression>)

<derived expression> -> (cond <cond clause>+)
                      | (cond <cond clause>* (else <sequence>))
                      | (case <expression> <case clause>+)
                      | (case <expression> <case clause>* (else <sequence>))
                      | (and <test>*)
                      | (or <test>*)
                      | (let (<binding spec>*) <body>)
                      | (let <variable (<binding spec>*) <body>)
                      | (let* (<binding spec>*) <body>)
                      | (letrec (<binding spec>*) <body>)
                      | (begin <sequence>)
                      | (do (<iteration spec>*) (<test> <sequence>) <command>*)
                      | (delay <expression>)
                      | <quasiquotation>

<cond clause> -> (<test> <sequence>)
               | (else <sequence>)
               | (<test>)
               | (<test> => <recipient>)
<recipient> -> <expression>
<case clause> -> ((<datum>*) <sequence>)
               | (else <sequence>)

<binding spec> -> (<variable> <expression>)
<iteration spec> -> (<variable> <init> <step>)
                  | (<variable> <init>)
<init> -> <expression>
<step> -> <expression>
\end{Verbatim}

\subsection*{Programas y definiciones}
Esta es la gramática de los programas que se compilarán:

\begin{Verbatim}[fontsize=\small]
<program> -> <command or definition>*
<command or definition> -> <command> | <definition>
<definition> -> (define <variable> <expression>)
              | (define (<variable> <def formals>) <body>)
<def formals> -> <variable>*
               | <variable>* . <variable>
\end{Verbatim}

\end{document}