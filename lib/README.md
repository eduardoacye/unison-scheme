# Bibliotecas de Scheme

Este directorio contiene las bibliotecas de Scheme usadas por el sistema.

# uson

En la biblioteca `uson` está el código de las herramientas utilizadas en varias partes del proyecto, cuando una herramienta es utilizada de manera específica para una componente del sistema, ésta se mueve a otra biblioteca.

Actualmente `uson` se compone de:
* `(uson character-set)`
* `(uson hash-table)`
* `(uson integer-bitwise-operation)`
* `(uson regular-expressions)`
