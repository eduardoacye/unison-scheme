;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Based on SRFI 69 by Panu Kalliokoski
;;
;; Tested on
;; * chibi-scheme   DONE CUSTOM
;; * larceny        DONE SRFI-78
;; * sagittarius    DONE SRFI-78
;; * kawa           TODO CUSTOM
;; * gauche         TODO CUSTOM

;; The following imports will be removed when the code is completely implemented
;(import (scheme base))
;(import (scheme char))
;(import (scheme case-lambda))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE RECORD TYPE DEFINITION ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; record-type <hash-table>
;;
;; @size: number of elements stored in the hash table
;; @hash: procedure that maps (key -> unsigned-integer)
;; @compare: procedure that maps (obj1 x obj2 -> boolean)
;; @associate: procedure that maps (obj x alist -> boolean | pair)
;; @entries: vector of alists
;;
;; The structure of the hash table is a vector of association lists, an association
;; list is a list of pairs, each pair containing the key and the mapped value. The
;; alists are used to do chaining in the table.
(define-record-type <hash-table>
  (%make-hash-table size hash compare associate entries)
  hash-table?
  (size       hash-table-size hash-table-set-size!)
  (hash       hash-table-hash-function)
  (compare    hash-table-equivalence-function)
  (associate  hash-table-association-function)
  (entries    hash-table-entries hash-table-set-entries!))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASHING AND HASH RELATED PROCEDURES ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; generic hash function
;;
;; This procedure checks the kind of object it has to hash to decide an appropriate
;; hash function for that kind of datum.
;;
;; @obj:
;; @bound:
;;
;; @default bound: *default-bound*
(define hash
  (case-lambda
   ((obj)       (hash obj *default-bound*))
   ((obj bound) (cond ((integer? obj)
                       (integer-hash obj bound))
                      ((string? obj)
                       (string-hash obj bound))
                      ((symbol? obj)
                       (symbol-hash obj bound))
                      ((real? obj)
                       (real-hash obj bound))
                      ((complex? obj)
                       (complex-hash obj bound))
                      ((char? obj)
                       (char-hash obj bound))
                      ((vector? obj)
                       (vector-hash obj bound))
                      ((pair? obj)
                       (pair-hash obj bound))
                      ((null? obj) 0)
                      ((not obj) 0)
                      ((procedure? obj)
                       (error "procedure hash: procedures cannot be hashed" obj))
                      (else 1)))))

;; default bound, it's an arbirary big possitive integer
(define *default-bound* (- (expt 2 29) 3))

;; alias for the hash procedure, if desired, could be optimized (in the underlying implementation)
;; as a hashing function for pointers and pointer comparison.
(define hash-by-identity
  (case-lambda
   ((obj)       (hash obj *default-bound*))
   ((obj bound) (hash obj bound))))

;; hash function for strings
(define string-hash
  (case-lambda
   ((obj)       (string-hash obj *default-bound*))
   ((obj bound) (%string-hash obj (lambda (x) x) bound))))

(define string-ci-hash
  (case-lambda
   ((obj)       (string-ci-hash obj *default-bound*))
   ((obj bound) (%string-hash obj char-downcase bound))))

(define symbol-hash
  (case-lambda
   ((obj)       (symbol-hash obj *default-bound*))
   ((obj bound) (%string-hash (symbol->string obj) (lambda (x) x) bound))))

(define (%string-hash str char-transform bound)
  (let ((hash 31)
        (len (string-length str)))
    (do ((index 0 (+ index 1)))
        ((>= index len) (modulo hash bound))
      (set! hash (modulo (+ (* 31 hash)
                            (char->integer (char-transform (string-ref str index))))
                         *default-bound*)))))

(define integer-hash
  (case-lambda
   ((obj)       (integer-hash obj *default-bound*))
   ((obj bound) (exact (modulo obj bound)))))

(define real-hash
  (case-lambda
   ((obj)       (real-hash obj *default-bound*))
   ((obj bound) (exact (modulo (+ (numerator obj) (denominator obj)) bound)))))

(define complex-hash
  (case-lambda
   ((obj)       (complex-hash obj *default-bound*))
   ((obj bound) (modulo (+ (hash (real-part obj))
                           (* 3 (hash (imag-part obj))))
                        bound))))

(define char-hash
  (case-lambda
   ((obj)       (char-hash obj *default-bound*))
   ((obj bound) (integer-hash (char->integer obj) bound))))

(define vector-hash
  (case-lambda
   ((obj)       (vector-hash obj *default-bound*))
   ((obj bound) (let ((hashvalue 571)
                      (len (vector-length obj)))
                  (do ((index 0 (+ index 1)))
                      ((>= index len) (modulo hashvalue bound))
                    (set! hashvalue
                          (modulo (+ (* 257 hashvalue)
                                     (hash (vector-ref obj index)))
                                  *default-bound*)))))))

(define pair-hash
  (case-lambda
   ((obj)       (pair-hash obj *default-bound*))
   ((obj bound) (modulo (+ (hash (car obj)) (* 3 (hash (cdr obj))))
                        bound))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE CONSTRUCTORS ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; hash-table constructor
;;
;; This procedure constructs a <hash-table> record given
;; @same?: procedure that maps (obj1 x obj2 -> boolean) it says if obj1 and obj2 are the same
;;  by some criteria.
;; @hash: procedure that maps (key -> unsigned-integer).
;; @size: number that determines the size of the hash-table vector.
;;
;; @default same?: equal?
;; @default hash: (appropriate-hash-function-for same?)
;; @default size: *default-table-size*
(define make-hash-table
  (case-lambda
   (()
    (make-hash-table equal?))
   ((same?)
    (make-hash-table same? (appropriate-hash-function-for same?)))
   ((same? hash)
    (make-hash-table same? hash *default-table-size*))
   ((same? hash size)
    (let ((associate (cond ((eq? same? eq?) assq)
                           ((eq? same? eqv?) assv)
                           ((eq? same? equal?) assoc)
                           (else (lambda (val alist) (assoc val alist same?))))))
      (%make-hash-table 0 hash same? associate (make-vector size '()))))))

;; the size of the vector of the hash-tables with unspecified size.
;; arbitrary value (expt 2 6)
(define *default-table-size* 64)

;; appropiate selector of hash function based on comparator.
;;
;; This procedure inspects the argument same? to determine what
;; hash function will be more suitable for a hash-table with the
;; given comparator.
;;
;; @same?: procedure that maps (obj1 x obj2 -> boolean) determines is obj1 and obj2
;; are the same in some sense.
(define (appropriate-hash-function-for same?)
  (cond ((eq? same? eq?) hash-by-identity)
        ((eq? same? string=?) string-hash)
        ((eq? same? string-ci=?) string-ci-hash)
        (else hash)))

;; Useful hash-table specific type constructors
;;
;; * hash-table with symbols as keys
;; * hash-table with strings as keys
;; * hash-table with case insensitive strings as keys
;; * hash-table with integers as keys

(define (make-symbol-hash-table size)
  (make-hash-table eq? symbol-hash size))

(define (make-string-hash-table size)
  (make-hash-table string=? string-hash size))

(define (make-string-ci-hash-table size)
  (make-hash-table string-ci=? string-ci-hash size))

(define (make-integer-hash-table size)
  (make-hash-table = integer-hash size))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE OPERATIONS ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; get the value associated to the given key in the given hash-table.
;;
;; @hash-table: a hash table.
;; @key: a key object for the given hash table.
;; @maybe-default
(define hash-table-ref
  (case-lambda
   ((hash-table key)
    (hash-table-ref hash-table key
                    (lambda () (error "procedure hash-table-ref: no value associated with" key))))
   ((hash-table key default-thuck)
    (cond ((%hash-table-find (hash-table-entries hash-table)
                             (hash-table-association-function hash-table)
                             (%hash-table-hash hash-table key)
                             key)
           => %hash-node-value)
          (else (default-thuck))))))


(define (hash-table-ref/default hash-table key default)
  (hash-table-ref hash-table key (lambda () default)))

(define (hash-table-set! hash-table key value)
  (let ((idx (%hash-table-hash hash-table key))
        (entries (hash-table-entries hash-table)))
    (cond ((%hash-table-find entries
                             (hash-table-association-function hash-table)
                             idx
                             key)
           => (lambda (node) (%hash-node-set-value! node value)))
          (else (%hash-table-add! entries idx key value)
                (hash-table-set-size! hash-table
                                      (+ 1 (hash-table-size hash-table)))
                (%hash-table-maybe-resize! hash-table)))))

(define (hash-table-delete! hash-table key)
  (if (%hash-table-delete! (hash-table-entries hash-table)
                           (hash-table-equivalence-function hash-table)
                           (%hash-table-hash hash-table key)
                           key)
      (hash-table-set-size! hash-table (- (hash-table-size hash-table) 1))))

(define (hash-table-exists? hash-table key)
  (and (%hash-table-find (hash-table-entries hash-table)
                         (hash-table-association-function hash-table)
                         (%hash-table-hash hash-table key)
                         key)
       #true))

(define hash-table-update!
  (case-lambda
   ((hash-table key function)
    (hash-table-update! hash-table key function
                        (lambda () (error "procedure hash-table-update!: no value exists fore key" key))))
   ((hash-table key function default-thuck)
    (let ((idx (%hash-table-hash hash-table key))
          (entries (hash-table-entries hash-table)))
      (cond ((%hash-table-find entries
                               (hash-table-association-function hash-table)
                               idx key)
             => (lambda (node)
                  (%hash-node-set-value! node
                                         (function (%hash-node-value node)))))
            (else (%hash-table-add! entries idx key
                                    (function (default-thuck)))
                  (hash-table-set-size! hash-table (+ 1 (hash-table-size hash-table)))
                  (%hash-table-maybe-resize! hash-table)))))))

(define (hash-table-update!/default hash-table key function default)
  (hash-table-update! hash-table key function (lambda () default)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; AUXILARY PROCEDURES FOR HASH-TABLE OPERATIONS ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (%hash-table-hash hash-table key)
  ((hash-table-hash-function hash-table)
   key (vector-length (hash-table-entries hash-table))))

(define (%hash-table-find entries associate hash key)
  (associate key (vector-ref entries hash)))

(define (%hash-table-add! entries idx key value)
  (vector-set! entries idx
               (cons (%make-hash-node key value)
                     (vector-ref entries idx))))

(define (%hash-table-delete! entries same? idx key)
  (let ((entry-list (vector-ref entries idx)))
    (cond ((null? entry-list) #false)
          ((same? key (caar entry-list))
           (vector-set! entries idx (cdr entry-list)) #true)
          (else
           (let loop ((current (cdr entry-list)) (previous entry-list))
             (cond ((null? current) #false)
                   ((same? key (caar current))
                    (set-cdr! previous (cdr current)) #true)
                   (else (loop (cdr current) current))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE (KEY . VALUE) NODES OPERATIONS ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (%make-hash-node key value)
  (cons key value))

(define (%hash-node-set-value! node value)
  (set-cdr! node value))

(define (%hash-node-key node)
  (car node))

(define (%hash-node-value node)
  (cdr node))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE PROCEDURES ON CONTENT ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (hash-table-walk hash-table proc)
  (%hash-table-walk
   (lambda (node) (proc (%hash-node-key node) (%hash-node-value node)))
   (hash-table-entries hash-table)))

(define (hash-table-fold hash-table function acc)
  (hash-table-walk hash-table
                   (lambda (key value) (set! acc (function key value acc))))
  acc)

(define (hash-table-copy hash-table)
  (let ((new (make-hash-table (hash-table-equivalence-function hash-table)
                              (hash-table-hash-function hash-table)
                              (max *default-table-size*
                                   (* 2 (hash-table-size hash-table))))))
    (hash-table-walk hash-table
                     (lambda (key value) (hash-table-set! new key value)))
    new))

(define (hash-table-merge! hash-table1 hash-table2)
  (hash-table-walk
   hash-table2
   (lambda (key value) (hash-table-set! hash-table1 key value)))
  hash-table1)

(define (hash-table-keys hash-table)
  (hash-table-fold hash-table (lambda (key val acc) (cons key acc)) '()))

(define (hash-table-values hash-table)
  (hash-table-fold hash-table (lambda (key val acc) (cons val acc)) '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; AUXILIARY PROCEDURES FOR HASH-TABLE PROCEDURES ON CONTENT ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (%hash-table-walk proc entries)
  (do ((index (- (vector-length entries) 1) (- index 1)))
      ((< index 0))
    (for-each proc (vector-ref entries index))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE CONVERSION ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define alist->hash-table
  (case-lambda
   ((alist)            (alist->hash-table alist equal?))
   ((alist same?)      (alist->hash-table alist same? (appropriate-hash-function-for same?)))
   ((alist same? hash) (alist->hash-table alist same? hash (max *default-table-size* (* 2 (length alist)))))
   ((alist same? hash size)
    (let ((hash-table (make-hash-table same? hash size)))
      (for-each
       (lambda (elem)
         (hash-table-update!/default hash-table (car elem) (lambda (x) x) (cdr elem)))
       alist)
      hash-table))))

(define (hash-table->alist hash-table)
  (hash-table-fold hash-table
                   (lambda (key val acc) (cons (cons key val) acc)) '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; HASH-TABLE AUTOMATIC RESIZING ;; ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (%hash-table-maybe-resize! hash-table)
  (let* ((old-entries (hash-table-entries hash-table))
         (table-size  (vector-length old-entries)))
    (if (> (hash-table-size hash-table) table-size)
        (let* ((new-size (* 2 table-size))
               (new-entries (make-vector new-size '()))
               (hash-function (hash-table-hash-function hash-table)))
          (%hash-table-walk
           (lambda (node)
             (%hash-table-add! new-entries
                               (hash (%hash-node-key node) new-size)
                               (%hash-node-key node) (%hash-node-value node)))
           old-entries)
          (hash-table-set-entries! hash-table new-entries)))))
