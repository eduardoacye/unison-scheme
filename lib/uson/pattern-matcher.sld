;;; -*- coding: utf-8; mode: scheme -*-
;;; 2015 - Eduardo Acuña Yeomans
;;;
;;; pattern matching based on sicp
;;; ==============================
;;;

(define-library (uson pattern-matcher)
  (export
   simplifier
   )
  (import (scheme base)
	  (scheme eval)
	  (scheme repl))
  (include "./pattern-matcher/simplifier.scm"))
