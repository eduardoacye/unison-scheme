;;; -*- coding: utf-8; mode; scheme -*-
;;; 2015 - Eduardo Acuña Yeomans
;;;
;;; regular expression language
;;; ===========================
;;;
;;; matching
;;; --------

(define regexp-deriv
  '(
    ;; nullable rules
    ( (nf #t)                  #t                        )
    ( (nf (?c a))              #f                        )
    ( (nf #f)                  #f                        )
    ( (nf (.. (? r) (? s)))    (& (nf (@ r)) (nf (@ s))) )
    ( (nf (: (? r) (? s)))     (: (nf (@ r)) (nf (@ s))) )
    ( (nf (* (? r)))           #t                        )
    ( (nf (& (? r) (? s)))     (& (nf (@ r)) (nf (@ s))) )
    ( (nf (~ (? r)))           (~ (nf (@ r)))            )

    ;; derivative rules
    ( (dd #t (?c a))                 #f                                   )
    ( (dd (?c a) (? a))              #t                                   )
    ( (dd (?c a) (?c b))             #f                                   )
    ( (dd #f (?c a))                 #f                                   )
    ( (dd (.. (? r) (? s)) (?c a))   (: (.. (dd (@ r) (@ a)) (@ s))
					(.. (nf (@ r)) (dd (@ s) (@ a)))) )
    ( (dd (: (? r) (? s)) (?c a))    (: (dd (@ r) (@ a))
					(dd (@ s) (@ a)))                 )
    ( (dd (* (? r)) (?c a))          (.. (dd (@ r) (@ a)) (* (@ r)))      )
    ( (dd (& (? r) (? s)) (?c a))    (& (dd (@ r) (@ a))
					(dd (@ s) (@ a)))                 )
    ( (dd (~ (? r)) (?c a))          (~ (dd (@ r) (@ a)))                 )
    ))

(define rderiv (simplifier regexp-deriv))


;;; The algebraic transformations for regular expressions are going to favor
;;; reduction to value and maximum factorization against order of arguments
(define regexp-algebra
  '(
    ;; Associativity laws
    ( (:  (:  (? L) (? M)) (? N))   (:  (@ L) (:  (@ M) (@ N))) )
    ( (.. (.. (? L) (? M)) (? N))   (.. (@ L) (.. (@ M) (@ N))) )
    ( (&  (&  (? L) (? M)) (? N))   (&  (@ L) (&  (@ M) (@ N))) )

    ;; Identity and Annihilators laws
    ( (:  #f (? L))   (@ L) )
    ( (:  (? L) #f)   (@ L) )
    ( (.. #f (? L))   #f    )
    ( (.. (? L) #f)   #f    )
    ( (&  #f (? L))   #f    )
    ( (&  (? L) #f)   #f    )

    ( (.. #t (? L))   (@ L) )
    ( (.. (? L) #t)   (@ L) )
    ( (&  #t (? L))   #t    )
    ( (&  (? L) #t)   #t    )

    ;; Distributive laws
    ( (: (.. (? L) (? M)) (.. (? L) (? N)))   (.. (@ L) (: (@ M) (@ N))) )
    ( (: (.. (? M) (? L)) (.. (? N) (? L)))   (.. (: (@ M) (@ N)) (@ L)) )

    ( (& (.. (? L) (? M)) (.. (? L) (? N)))   (.. (@ L) (& (@ M) (@ N))) )
    ( (& (.. (? M) (? L)) (.. (? N) (? L)))   (.. (& (@ M) (@ N)) (@ L)) )

    ( (: (& (? L) (? M)) (& (? L) (? N)))     (& (@ L) (: (@ M) (@ N)))  )
    ( (: (& (? M) (? L)) (& (? N) (? L)))     (& (: (@ M) (@ N)) (@ L))  )

    ;; Idempotent laws
    ( (: (? L) (? L))   (@ L) )
    ( (& (? L) (? L))   (@ L) )

    ;; Closure laws
    ( (* (* (? L)))          (* (@ L))            )
    ( (* #f)                 #t                   )
    ( (* #t)                 #t                   )
    ( (.. (* (? L)) (? L))   (.. (@ L) (* (@ L))) )

    ;; Logic laws
    ( (~ (~ (? L)))                             (@ L) )
    
    ( (: (& (? L) (? M)) (& (? L) (~ (? M))))   (@ L) )
    ( (: (& (? L) (~ (? M))) (& (? L) (? M)))   (@ L) )
    
    ( (& (: (? L) (? M)) (: (? L) (~ (? M))))   (@ L) )
    ( (& (: (? L) (~ (? M))) (: (? L) (? M)))   (@ L) )
    
    ( (: (? L) (& (? L) (? M)))                 (@ L) )
    ( (: (? L) (& (? M) (? L)))                 (@ L) )

    ( (& (? L) (: (? L) (? M)))                 (@ L) )
    ( (& (? L) (: (? M) (? L)))                 (@ L) )

    ( (& (? L) (~ (? L)))                       #f    )
    ( (& (~ (? L)) (? L))                       #f    )
    ))

(define rsimp (simplifier regexp-algebra))

(define (: a b)
  (or a b))

(define (& a b)
  (and a b))

(define (~ a)
  (not a))

(define (preprocess exp)
  (define (transform lst op id)
    (if (null? lst)
	id
	(list op (preprocess (car lst)) (transform (cdr lst) op id))))

  (cond ((null? exp) '())
	((pair? exp)
	 (case (car exp)
	   ((:)  (transform (cdr exp) ':  #f))
	   ((..) (transform (cdr exp) '.. #t))
	   ((&)  (transform (cdr exp) '&  #t))
	   ((~ *) (list (car exp) (preprocess (cadr exp))))))
	((string? exp) (transform (string->list exp) '.. #t))
	((char? exp) exp)
	((boolean? exp) exp)
	(else (error "not a valid regular expression" exp))))

(define-syntax re
  (syntax-rules ()
    ((re exp)
     (rsimp (preprocess (quote exp))))))

(define (rederiv re c)
  (rsimp (rderiv (list 'dd (rsimp re) c))))

(define (rederiv* re s)
  (let loop ((lst (reverse (string->list s))))
    (if (null? lst)
	(rsimp re)
	(rsimp (rderiv (list 'dd (loop (cdr lst)) (car lst)))))))

(define (nullable? re)
  (eval (rderiv (list 'nf (rsimp re))) (interaction-environment)))

(define (matches? re s)
  (nullable? (rederiv* re s)))
