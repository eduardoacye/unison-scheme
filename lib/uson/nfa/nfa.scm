;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Record type definitions ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-record-type <nfa>
  (make-nfa start: final:)
  nfa?
  (start: start set-start!)
  (final: final set-final!))

(define-record-type <transition>
  (make-transition input: goto-qs:)
  transition?
  (input: transition-input)
  (goto-qs: transition-qs set-transition-qs!))

(define-record-type <state>
  (%make-state id: trans:)
  state?
  (id: state-id)
  (trans: state-trans set-state-trans!))

;;;;;;;;;;;;;;;;;;;;;;;
;; Record procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; <nfa>
(define (add-final! nfa q) (set-final! nfa (cons q (final nfa))))

(define (remove-final! nfa q) (set-final! nfa (remove (lambda (x) (eq? x q)) (final nfa))))

(define (final? nfa q) (if (memq q (final nfa)) #t #f))

(define (start? nfa q) (eq? q (start nfa)))

;; <transition>
(define (lookup-trans ts i)
  (cond ((null? ts) #f)
        ((eq? (transition-input (car ts)) i) (car ts))
        (else (lookup-trans (cdr ts) i))))

(define (lookup-trans-qs ts i)
  (define trans (lookup-trans ts i))
  (if trans (transition-qs trans) '()))

;; <state>
(define (state-ids . qs) (map state-id qs))

(define (state-add-trans! q i p)
  (cond ((lookup-trans (state-trans q) i)
         => (lambda (t) (set-transition-qs! t (lset-adjoin eq? (transition-qs t) p))))
        (else (set-state-trans! q (cons (make-transition i (list p)) (state-trans q))))))

(define (state-remove-trans! q i p)
  (define t (lookup-trans (state-trans q) i))
  (when t (set-transition-qs! t (remove (lambda (x) (eq? x p)) (transition-qs t)))))

(define (make-state id)
  (define q (%make-state id '()))
  (state-add-trans! q '() q)
  q)

;; Running nfas
(define (delta q i)
  (define (input-to r) (lookup-trans-qs (state-trans r) i))
  (define (epsilon-to r) (lookup-trans-qs (state-trans r) '()))
  (define (instant-states qs)
    (define ps (apply lset-union (cons eq? (cons qs (map epsilon-to qs)))))
    (if (lset= eq? ps qs) qs (instant-states ps)))
  (define instants (instant-states (list q)))
  (apply lset-union (cons eq? (map input-to instants))))

(define (run-nfa nfa inputs)
  (define (step states inputs)
    (cond ((null? states) #f)
          ((null? inputs)
           (let ((final-qs (filter (lambda (q)
                                     (final? nfa q))
                                   (apply lset-union (cons eq? (map (lambda (q) (delta q '())) states))))))
             (if (null? final-qs)
                 #f
                 final-qs)))
          (else (step (apply lset-union (cons eq? (map (lambda (state) (delta state (car inputs))) states)))
                      (cdr inputs)))))
  (step (list (start nfa)) inputs))
