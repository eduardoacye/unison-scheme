;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;

;; Regular expression syntax:
;; ==========================
;; * any string "a" is a regexp
;; * the empty string "" denotes ɛ
;; * Ri 0<i<=n are regexps -> the union (quote (: R1 R2 ... Rn)) is a regexp
;; * Ri 0<i<=n are regexps -> the concatenation (quote (.. R1 R2 ... Rn)) is a regexp
;; * R is a regexp -> the kleene star (quote (* R)) is a regexp
;; * R is a regexp -> the possitive star (quote (+ R)) is a regexp
;; * R is a regexp -> maybe a regexp (quote (? R)) is a regexp
;; * S is a symbol & S is defined in env -> S is a regexp
;;
;; Regular expression construction:
;; ================================
;; A regular expression is created with the regexp procedure, it consists
;; of two elements:
;; * a regexp specification with the syntax described above
;; * a rib-cage environment of the form:
;;      (<symbols> . <sets>)
;;   where <symbols> is a list of length n that contains unique symbols and
;;  <sets> is a list of length n that that contains sets of characters
;;

(define-record-type <regexp>
  (%make-regexp spec: env:)
  regexp?
  (spec: spec)
  (env:  env set-env!))

(define make-regexp
  (case-lambda
   ((spec) (make-regexp spec '()))
   ((spec env) (%make-regexp spec env))))

(define (env-lookup regexp sym)
  (define e (env regexp))
  (let lookup ((syms (car e))
               (sets (cdr e)))
    (cond ((or (null? syms) (null? sets)) #f)
          ((eq? (car syms) sym) (car sets))
          (else (lookup (cdr syms) (cdr sets))))))
