;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Based on SRFI 69 by Panu Kalliokoski
;;

(define-library (uson hash-table)
  (export make-hash-table
          hash-table?
          alist->hash-table

          hash-table-equivalence-function
          hash-table-hash-function

          hash-table-ref
          hash-table-ref/default
          hash-table-set!
          hash-table-delete!
          hash-table-exists?
          hash-table-update!
          hash-table-update!/default

          hash-table-size
          hash-table-keys
          hash-table-values
          hash-table-walk
          hash-table-fold
          hash-table->alist
          hash-table-copy
          hash-table-merge!

          hash
          string-hash
          string-ci-hash
          hash-by-identity
          )
  (import (scheme base)
          (scheme char)
          (scheme case-lambda)
          (scheme complex))
  (include "hash-table/hash-table.scm"))
