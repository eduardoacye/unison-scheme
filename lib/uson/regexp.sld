;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Based on Donald Rodriguez lectures and reading  Thompson construction algorithm and cinderella book
;;
;; The procedure for compiling regular expressions is regexp -> NFA -> DFA -> min DFA -> transition table

;; Primitives:
;; ɛ     -> ><
;; a     -> a
;; R1|R2 -> (: R1 R2)
;; R1R2  -> (.. R1 R2)
;; R*    -> (* R)
;;
;; Derived:
;; R?    -> (? R)
;; R+    -> (+ R)
;;
;; Predefined sets:
;; alphabetic
;; lower-case
;; numeric
;; upper-case
;; whitespace

(define-library (uson regexp)
  (export
   ;; <regexp>
   make-regexp regexp? spec env set-env!
   env-lookup
   )
  (import (scheme base)
          (scheme char)
          (scheme case-lambda)
          (prefix (uson nfa) nfa:))
  (include "regexp/regexp.scm"))
