;;; -*- coding: utf-8; mode: scheme -*-
;;; 2015 - Eduardo Acuña Yeomans
;;;
;;; pattern matching and rule based substitution based on sicp
;;; ==========================================================
;;;
;;; simplifier
;;; ----------


;;;;;;;;;;;;;;;;
;; Dictionary ;;
;;;;;;;;;;;;;;;;

;; The dictionary is an association list
;; -------------------------------------

(define (empty-dictionary)
  (list))

(define (lookup key dictionary)
  (cond ((assq key dictionary) => cadr)
	(else key)))

(define (extend-dictionary name expression dictionary)
  (cond ((assq name dictionary) =>
	 (lambda (p) (if (equal? (cadr p) expression) dictionary 'failed)))
	(else (cons (list name expression) dictionary))))

;;;;;;;;;;;;;
;; Matcher ;;
;;;;;;;;;;;;;

;; The pattern matcher DSL
;; -----------------------

;; foo					        ; matches foo
;; (a b c)					; matches a three list with elements a b and c
;; (?v x)					; matches a symbol x
;; (?c x)					; matches a simple datum but not a symbol
;; (? x)					; matches any expression

;;; An atom is a simple datum (R7RS datum syntax)
(define (atom? obj)
  (or (symbol? obj)
      (boolean? obj)
      (number? obj)
      (char? obj)
      (string? obj)
      (bytevector? obj)))

;;; A constant pattern is something of the form (?c x) where x is a symbol
(define (constant-pattern? pattern)
  (and (pair? pattern)
       (eq? (car pattern) '?c)
       (not (null? (cdr pattern)))
       (symbol? (cadr pattern))))

;;; The name of a constant (?c x) is the symbol x
(define (constant-name pattern)
  (cadr pattern))

;;; A constant expression is an atom that is not a symbol
(define (constant-expression? expression)
  (and (not (symbol? expression))
       (atom? expression)))

;;; A variable pattern is something of the form (?v x) where x is a symbol
(define (variable-pattern? pattern)
  (and (pair? pattern)
       (eq? (car pattern) '?v)
       (not (null? (cdr pattern)))
       (symbol? (cadr pattern))))

;;; The name of a variable (?v x) is the symbol x
(define (variable-name pattern)
  (cadr pattern))

;;; A variable expression is a symbol
(define (variable-expression? expression)
  (symbol? expression))

;;; An any pattern is something of the form (? x) where x is a symbol
(define (any-pattern? pattern)
  (and (pair? pattern)
       (eq? (car pattern) '?)
       (not (null? (cdr pattern)))
       (symbol? (cadr pattern))))

;;; The name of an any (? x) is the symbol x
(define (any-name pattern)
  (cadr pattern))

;;; Return the given dictionary augmented with the matched sub-patterns associated
;;; with their corresponding sub-expressions or return a failure.
(define (match pattern expression dictionary)
  (cond ((eq? dictionary 'failed)
	 'failed)
	((and (null? pattern) (null? expression))
	 dictionary)
	((or (null? pattern) (null? expression))
	 'failed)
	((atom? pattern)
	 (if (atom? expression)
	     (if (equal? pattern expression)
		 dictionary
		 'failed)
	     'failed))
	((constant-pattern? pattern)
	 (if (constant-expression? expression)
	     (extend-dictionary (constant-name pattern)
				expression
				dictionary)
	     'failed))
	((variable-pattern? pattern)
	 (if (variable-expression? expression)
	     (extend-dictionary (variable-name pattern)
				expression
				dictionary)
	     'failed))
	((any-pattern? pattern)
	 (extend-dictionary (any-name pattern)
			    expression
			    dictionary))
	((atom? expression)
	 'failed)
	(else
	 (match (cdr pattern)
		(cdr expression)
		(match (car pattern)
		       (car expression)
		       dictionary)))))


;;;;;;;;;;;;;;;;;;
;; Instantiator ;;
;;;;;;;;;;;;;;;;;;

;; The skeleton instantiator DSL
;; -----------------------------

;; foo                                          ; instantiates to foo
;; (a b c)                                      ; instantiates to a list with the instantiations of a, b and c
;; (@ x)                                        ; instantiates to the value of x in the matched pattern

(define (skeleton-evaluation? skeleton)
  (and (pair? skeleton)
       (eq? (car skeleton) '@)
       (not (null? (cdr skeleton)))))

(define (evaluation-expression skeleton)
  (cadr skeleton))

(define (evaluate expression dictionary)
  (cond ((symbol? expression)
	 (lookup expression dictionary))
	((atom? expression)
	 (eval expression (interaction-environment)))
	(else
	 (apply (eval (lookup (car expression) dictionary)
		      (interaction-environment))
		(map (lambda (arg) (lookup arg dictionary))
		     (cdr expression))))))

(define (instantiate skeleton dictionary)
  (cond ((null? skeleton)
	 '())
	((atom? skeleton)
	 skeleton)
	((skeleton-evaluation? skeleton)
	 (evaluate (evaluation-expression skeleton) dictionary))
	(else (cons (instantiate (car skeleton) dictionary)
		    (instantiate (cdr skeleton) dictionary)))))


;;;;;;;;;;;;;;;;
;; Simplifier ;;
;;;;;;;;;;;;;;;;

;; General purpuse simplifier interface
;; ------------------------------------

;; The procedure simplifier receives some rules and returns the procedure
;; simplify-expression, this returned procedure receives an expression and
;; returns a simplified version of it.

;; The rules DSL 
;; -------------

;; The rules is a list (rule-1 rule-2 rule-3 ... rule-n).

;; Each rule is a list (pattern skeleton). The pattern corresponds to a
;; program written in the pattern matcher DSL and the skeleton corresponds
;; to a program written in the skeleton instantiator DSL.

;; How does the simplifier works
;; -----------------------------

;; Each rule is tried in order with an expression until one matches or the
;; expression isnt a match with with any of them, in the former case the
;; expression is simplified acording to the rule and in the latter case the
;; expression is returned.

(define (rule-pattern rule)
  (car rule))

(define (rule-skeleton rule)
  (cadr rule))

(define (simplifier rules)
  (define (simplify-expression expression)
    (try-rules (if (pair? expression)
		   (map simplify-expression expression)
		   expression)))
  (define (try-rules expression)
    (define (scan rules)
      (if (null? rules)
	  expression
	  (let* ((rule (car rules))
		 (pattern (rule-pattern rule))
		 (skeleton (rule-skeleton rule))
		 (dictionary (match pattern expression (empty-dictionary))))
	    (if (eq? dictionary 'failed)
		(scan (cdr rules))
		(simplify-expression (instantiate skeleton dictionary))))))
    (scan rules))
  simplify-expression)


;;;;;;;;;;;;;;;;;;
;; Sample rules ;;
;;;;;;;;;;;;;;;;;;

(define deriv-rules
  '(
    ( (dd (?c c) (? v))              0                                  )
    ( (dd (?v v) (? v))              1                                  )
    ( (dd (?v u) (? v))              0                                  )
    ( (dd (+ (? x1) (? x2)) (? v))   (+ (dd (@ x1) (@ v))
                                        (dd (@ x2) (@ v)))              )
    ( (dd (* (? x1) (? x2)) (? v))   (+ (* (@ x1) (dd (@ x2) (@ v)))
                                        (* (dd (@ x1) (@ v)) (@ x2)))   )
    ( (dd (** (? x) (?c n)) (? v))   (* (* (@ n) (+ (@ x) (@ (- n 1))))
                                        (dd (@ x) (@ v)))               )
    ))

(define algebra-rules
  '(
    ( ((? op) (?c c1) (?c c2))                  (@ (op c1 c2))               )
    ( ((? op) (?  e ) (?c c))                   ((@ op) (@ c) (@ e))         )
    ( (+ 0 (? e))                               (@ e)                        )
    ( (* 1 (? e))                               (@ e)                        )
    ( (* 0 (? e))                               0                            )
    ( (* (?c c1) (* (?c c2) (? e )))            (* (@ (* c1 c2)) (@ e))      )
    ( (* (?  e1) (* (?c c ) (? e2)))            (* (@ c ) (* (@ e1) (@ e2))) )
    ( (* (* (? e1) (? e2)) (? e3))              (* (@ e1) (* (@ e2) (@ e3))) )
    ( (+ (?c c1) (+ (?c c2) (? e )))            (+ (@ (+ c1 c2)) (@ e))      )
    ( (+ (?  e1) (+ (?c c ) (? e2)))            (+ (@ c ) (+ (@ e1) (@ e2))) )
    ( (+ (+ (? e1) (? e2)) (? e3))              (+ (@ e1) (+ (@ e2) (@ e3))) )
    ( (+ (* (?c c1) (? e)) (* (?c c2) (? e)))   (* (@ (+ c1 c2)) (@ e))      )
    ( (* (? e1) (+ (? e2) (? e3)))              (+ (* (@ e1) (@ e2)))        )
    ))

(define dsimp (simplifier deriv-rules))
(define alsimp (simplifier algebra-rules))

(define (deriv exp)
  (alsimp (dsimp exp)))
