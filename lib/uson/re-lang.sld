;;; -*- coding: utf-8; mode: scheme -*-
;;; 2015 - Eduardo Acuña Yeomans
;;;
;;; regular expression language
;;; ===========================
;;;

(define-library (uson re-lang)
  (export
   ;; from /re-lang/re-matching.scm
   & : ~
   rderiv rsimp
   re matches?
   )
  (import (scheme base)
	  (scheme eval)
	  (scheme repl)
	  (uson pattern-matcher))
  (include "./re-lang/re-matching.scm"))
