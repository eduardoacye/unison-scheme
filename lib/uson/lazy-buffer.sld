;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Functional lazy buffer for scanner development
;;

(define-library (uson lazy-buffer)
  (export
   first
   rest
   make-lazybuff
   from-input-file
   from-port
   )
  (import (scheme base)
	  (scheme file)
          (scheme lazy))
  (include "lazy-buffer/lazy-buffer.scm"))
