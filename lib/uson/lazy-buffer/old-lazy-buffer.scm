;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Functional lazy buffer for scanner development
;;
;; Tested on
;;

(import (scheme base)
        (scheme file)
        (scheme write))

(define (make-promise done? proc)
  (list (cons done? proc)))

(define (promise-done? x)
  (car (car x)))

(define (promise-value x)
  (cdr (car x)))

(define (promise-update! new old)
  (set-car! (car old) (promise-done? new))
  (set-cdr! (car old) (promise-value new))
  (set-car! new (car old)))

(define-syntax delay-force
  (syntax-rules ()
    ((delay-force expression)
     (make-promise #f (lambda () expression)))))

(define-syntax delay
  (syntax-rules ()
    ((delay expression)
     (delay-force (make-promise #t expression)))))

(define (force promise)
  (if (promise-done? promise)
      (promise-value promise)
      (let ((promise* ((promise-value promise))))
        (unless (promise-done? promise)
                (promise-update! promise* promise))
        (force promise))))

(define (first stream) (car (force stream)))
(define (rest stream) (cdr (force stream)))

(define (make-lazybuff port)
  (let next-char ((c (read-char port)))
    (if (eof-object? c)
        (delay (cons c '()))
        (delay (cons c (next-char (read-char port)))))))

(define-syntax from-input-file
  (syntax-rules ()
    ((from-input-file path proc)
     (call-with-input-file path
       (lambda (fp)
         (proc (make-lazybuff fp)))))))

(from-input-file
 "file-test.txt"
 (lambda (fs)
   (display (first fs))
   (newline)))

(define-syntax from-port
  (syntax-rules ()
    ((from-port port proc)
     (call-with-port port (lambda (p) (proc (make-lazybuff p)))))))

(from-port
 (open-input-string "bla lab abl")
 (lambda (ss)
   (display (first ss))
   (newline)))
