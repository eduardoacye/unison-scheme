;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Functional lazy buffer for scanner development
;;
;; Tested on
;;

(define (first stream) (car (force stream)))
(define (rest stream) (cdr (force stream)))

(define (make-lazybuff port)
  (let next-char ((c (read-char port)))
    (if (eof-object? c)
        (delay (cons c '()))
        (delay (cons c (next-char (read-char port)))))))

(define-syntax from-input-file
  (syntax-rules ()
    ((from-input-file path proc)
     (call-with-input-file path
       (lambda (fp)
         (proc (make-lazybuff fp)))))))

#;
(from-input-file
 "file-test.txt"
 (lambda (fs)
   (display (first fs))
   (newline)))

(define-syntax from-port
  (syntax-rules ()
    ((from-port port proc)
     (call-with-port port (lambda (p) (proc (make-lazybuff p)))))))

#;
(from-port
 (open-input-string "bla lab abl")
 (lambda (ss)
   (display (first ss))
   (newline)))
