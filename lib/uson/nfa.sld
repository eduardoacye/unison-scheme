;; -*- coding: utf-8; mode: scheme -*-
;; 2015 Eduardo Acuña Yeomans
;;
;; Based on Donald Rodriguez lectures and reading  Thompson construction algorithm and cinderella book
;;

;; This implementation of Non-deterministic finite automata requires the nfa have no unreachable states.
;;
;; A finite automaton is defined as a directed graph with a start
;; state and one or more final states. However for the thompson construction algorithm
;; the rule is to have every nfa with one starting state and one final state.
;;
;; * <nfa> is the record that holds the starting state and a list of the final states.
;; * <state> is the record that holds the id of a particlar state and a list of it's
;; transitions.
;; * <transition> is the record that holds the transition description for a state, it
;; holds an activation character and a list of go-to states.

(define-library (uson nfa)
  (export
   ;; <nfa>
   make-nfa nfa? start set-start! final set-final!
   add-final! remove-final! final? start?

   ;; <transition>
   make-transition transition? transition-input transition-qs set-transition-qs!
   lookup-trans lookup-trans-qs

   ;; <state>
   make-state state? state-id state-trans set-state-trans!
   state-add-trans! state-remove-trans!

   ;; running <nfas>
   delta run-nfa
   )
  (import (scheme base)
          (srfi 1))
  (include "nfa/nfa.scm"))
