# Biblioteca (uson)

## (uson hash-table)

Implementación de la **Scheme Request For Implementation 69** de Panu Kalliokoski, escrita como biblioteca de Scheme R7RS. La siguiente descripción es una traducción del resumen de la SRFI.

```
Esta SRFI define tablas hash básicas. Las tablas hash son ámpliamente reconocidas como una estructura de datos
fundamental para una grán variedad de aplicaciones. Una tabla hash es una estructura de datos que:

1. Provee un mapeo de algún conjunto de llaves a algún conjunto de valores asociados a estas llaves.
2. No tiene un orden intrínseco para las asociaciones (llave, valor) que almacena.
3. Soporta modificaciones en contexto como mecanismo principal de fijar su contenido.
4. Provee búsqueda por llaves y actualizaciones destructivas en un tiempo constante amortizado, dada una buena función hash.

Esta SRFI tiene las siguientes metas:

1. Proveer una API para tablas hash que sea consistente, genética y ampliamente aplicable.
2. Mejorar la portabilidad del código dando una facilidad estándar de tabla hash con un comportamiento garantizado.
3. Ayudar al programador definiendo rutinas de utilería que toman en cuenta las situaciones comunes del uso de las tablas hash.
```

## (uson nfa)

Implementación de autómatas finitos no deterministas, escrita como biblioteca de Scheme R7RS.

## (uson regexp)

Implementación de expresiones regulares con notación en prefijo basádo en las clases de Donald, la construcción de Thompson y el libro de automatones (Hopcroft, etal).

# Porcentaje de completado

* `(uson hashtable)` 100%
* `(uson nfa)` 100%
* `(uson regexp)` 25%
