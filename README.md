# unison-scheme

Implementación de Scheme desarrollada en la Universidad de Sonora.

## objetivos

Implementar el lenguaje de programación Scheme de manera clara, favoreciendo la sencillez sobre la eficiencia.

Al ser parte de un proyecto escolar, es de suma importancia mantener la complejidad bajo control. Toda la arquitectura y el funcionamiento del sistema debe ser entendible; aspector arcanos y truculentos serán evitados sin importar que tantos beneficios tenga.

## Estructura de los directorios

* `uson-scheme/` Directorio raíz
  * `README.md` Este archivo
  * `lib/` Directorio de bibliotecas de Scheme
    * `uson/`
      * `hash-table.sld`
      * `nfa.sld`
      * `regexp.sld`

## Arquitectura del sistema

El sistema está inspirado en el artículo de Rees y Kelsey *A Tractable Scheme Implementation* y la tesis de doctorado de Dybvig *Three implementation Models for Scheme*

El panorama general de la transformación del lenguaje a código ejecutable es como sigue:

1. Un programa escrito en **Scheme** es compilado a un equivalente que hace uso únicamente de formas sintácticas básicas al cual nos referiremos como **Primitive Scheme**.
2. Un programa escrito en **Primitive Scheme** es compilado a un ensablador.
3. El código en ensamblador es interpretado por una máquina virtual.

## Metas

### Corto plazo (hasta Diciembre 2015)

- **TODO** Programar el compilador de **Scheme** a **Primitive Scheme**.

- **TODO** Programar el compilador de **Primitive Scheme** a ensamblador.

- **TODO** Programar la máquina virtual.

### Mediano plazo (hasta Octubre 2016)

- **TODO** Pasar de Scheme a R7RS Scheme.

- **TODO** Programar las SRFI que parezcan útiles.

### Largo plazo (hasta que pierda sentido seguir trabajando en el proyecto)

